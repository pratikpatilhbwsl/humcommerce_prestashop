![alt tag](views/img/ha_logo.png)

## About

The ideal plugin for website visitor recordings, heatmaps, form analytics, A/B testing and more.

## Description 

Record, Analyze & Convert your website visitors - Using a single tool

## Paid Services 
Compare our [free and affordable plans](https://www.humcommerce.com/pricing?from###prestashop) or have a look at our [features](https://www.humcommerce.com/features?from###prestashop) to learn more.

## Get Started
Start recording, analyzing and converting your website visitors. [Install HumCommerce from our site](https://www.humcommerce.com/#signup?from###prestashop) in minutes.

## Installation 
1. Download plugin.
2. Go to Admin Dashboard -> Improve -> Modules -> Modules & Services -> Upload a module.
3. Configure the plugin through the 'Installed Plugins' menu in PrestaShop.

#### Manual Alternatives 
1. Extract & upload `humcommerce.zip` to the `/modules/` directory.
2. Configure the plugin through the 'Installed Plugins' menu in PrestaShop.

## Frequently Asked Questions ##

### Is HumCommerce free? ###
Yes! Try our [Beginner plan](https://www.humcommerce.com/pricing?from###wporg)  and have a look at our [features](https://www.humcommerce.com/features?from###wporg) to learn more.

### Should I purchase a paid plan? ###
HumCommerce’s paid services include more session recordings, more pageviews, priority support, and more.

To learn more about the services we provide, visit our [plan comparison page](https://www.humcommerce.com/pricing?from###wporg).

### What is Site ID? ###

Login to your HumCommerce Account. Go to My Account. Click on Launch Dashboard button.
The site id number is listed under Settings/Website/Manage in your HumDash administration panel.

### How do I view my stats? ###

Once you've installed HumCommerce, you can launch our tool from the “Launch Dashboard” button on My Account page.




