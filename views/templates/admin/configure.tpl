{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row" id="humcommerce_analytics_top">
		<div class="col-lg-6">
			<img src="{$module_dir|escape:'html':'UTF-8'}views/img/ha_logo.png" alt="Humcommerce Analytics"/>
		</div>
	</div>
	<hr/>
	<div id="humcommerce_analytics_content">
		<div class="row">
			<div class="col-lg-6">
				<p>
					<b>{l s='Record visitors on your site' mod='humcommerce'}</b>
				</p>
				<p>
					{l s='Select one of the options below to get started.' mod='humcommerce'}
				</p>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-lg-6">
				<p>
					<b>{l s='Activate HumCommerce. Log in or sign up now.' mod='humcommerce'}</b>
				</p>
				<p>
					<b><a href="https://www.humcommerce.com/#signup" rel="external"
						  target="_blank">{l s='Get your site ID' mod='humcommerce'}</a></b>
				</p>
			</div>
		</div>
	</div>
</div>
