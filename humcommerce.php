<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Humcommerce extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'humcommerce';
        $this->tab = 'analytics_stats';
        $this->version = '1.0.2';
        $this->author = 'humcommerce';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('HumCommerce');
        $this->description = $this->l('HumCommerce module to record, analyze and convert your website visitors.');

        $this->confirmUninstall = $this->l('Are you sure want to uninstall?');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('actionObjectCartAddAfter') &&
            $this->registerHook('actionObjectCartUpdateAfter') &&
            $this->registerHook('actionObjectCartDeleteAfter') &&
            $this->registerHook('actionValidateOrder');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitHumcommerceModule')) == true) {
            $this->postProcess();
        }

        $this->_path = __PS_BASE_URI__.'modules/'.$this->name.'/';

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitHumcommerceModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-tags"></i>',
                        'desc' => '(<a href="https://www.humcommerce.com/docs/find-site-id-humcommmerce-tool" 
                                    target="_blank"> What is SIte ID?</a>)',
                        'name' => 'HUMCOMMERCE_SITE_ID',
                        'label' => $this->l('Site ID'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'hidden',
                        'name' => 'HUMCOMMERCE_HOST',
                        'label' => $this->l('Host'),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $HUMCOMMERCE_HOST = Configuration::get('HUMCOMMERCE_HOST');
        if (empty($HUMCOMMERCE_HOST)) {
            $HUMCOMMERCE_HOST = 'app.humdash.com';
        }
        return array(
            'HUMCOMMERCE_SITE_ID' => Configuration::get('HUMCOMMERCE_SITE_ID', ''),
            'HUMCOMMERCE_HOST' => $HUMCOMMERCE_HOST
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        return "
            <!-- HumDash -->
            <script type=\"text/javascript\">
              var _ha = _ha || [];
              /* tracker methods like \"setCustomDimension\" should be called before \"trackPageView\" */
              _ha.push(['trackPageView']);
              _ha.push(['enableLinkTracking']);
              (function() {
                var u=\"//" . Configuration::get('HUMCOMMERCE_HOST') . "/\";
                _ha.push(['setTrackerUrl', u+'humdash.php']);
                _ha.push(['setSiteId', '".Tools::safeOutput(Configuration::get('HUMCOMMERCE_SITE_ID'))."']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; 
                g.async=true; 
                g.defer=true; 
                g.src=u+'humdash.js'; 
                s.parentNode.insertBefore(g,s);
              })();
            </script>
            <!-- End HumDash Code -->";
    }

    /**
    * Event Hander for Cart Add Hook
    */
    public function hookActionObjectCartAddAfter()
    {
        $this->addEcommerceCart();
    }
    /**
    * Event Hander for Cart Update Hook
    */
    public function hookActionObjectCartUpdateAfter()
    {
        $this->addEcommerceCart();
    }
    /**
    * Event Hander for Cart Add Hook
    */
    public function hookActionObjectCartDeleteAfter()
    {
        $this->addEcommerceCart();
    }
    /**
    * Add Ecommerce Cart
    */
    public function addEcommerceCart()
    {
        $humcommerce_site_id = Tools::safeOutput(Configuration::get('HUMCOMMERCE_SITE_ID'));
        $humcommerce_host = Tools::safeOutput(Configuration::get('HUMCOMMERCE_HOST'));
        if (empty($humcommerce_host)) {
            $humcommerce_host = 'app.humdash.com';
        }

        // Do the add to cart tracking on if HumCommerce site id added and host url is not empty
        if (!empty($humcommerce_site_id) && !empty($humcommerce_host)) {
            include_once(_PS_MODULE_DIR_.'/humcommerce/classes/HumcommerceApi.php');

            if ($this->context->cart) {
                $items = $this->context->cart->getProducts();
                $grandTotal = 0.0;
    
                $humcommerceTracker = new HumcommerceApi();
                $humcommerceTracker->init($humcommerce_site_id, 'https://' . $humcommerce_host);
    
                foreach ($items as $item) {
                    try {
                        $humcommerceTracker->addEcommerceItem(
                            $item['reference'],
                            $item['name'],
                            false,
                            $item['price'],
                            $item['cart_quantity']
                        );
                        $grandTotal += ($item['price'] * $item['cart_quantity']);
                    } catch (\Exception $exception) {
                    }
                }
                $humcommerceTracker->doTrackEcommerceCartUpdate($grandTotal);
            }
        }
    }
    
    public function hookActionValidateOrder($params)
    {
        if (!$this->active) {
            return;
        }
        
        $invalidStates = array(
            Configuration::get('PS_OS_REFUND'),
            Configuration::get('PS_OS_CANCELED')
        );

        $state = $params['order']->getCurrentState();
        if (!in_array($state, $invalidStates)) {
            $this->addEcommmerceOrder($params);
        }
    }
    
    public function addEcommmerceOrder($params)
    {
        $order = $params['order'];
        $cart = $params['cart'];
        
        $humcommerce_site_id = Tools::safeOutput(Configuration::get('HUMCOMMERCE_SITE_ID'));
        $humcommerce_host = Tools::safeOutput(Configuration::get('HUMCOMMERCE_HOST'));
        if (empty($humcommerce_host)) {
            $humcommerce_host = 'app.humdash.com';
        }

        if (!empty($humcommerce_site_id) && !empty($humcommerce_host)) {
            include_once(_PS_MODULE_DIR_.'/humcommerce/classes/HumcommerceApi.php');

            $orderItems = $order->getProducts();
            $humcommerceTracker = new HumcommerceApi();
            $humcommerceTracker->init($humcommerce_site_id, 'https://' . $humcommerce_host);

            foreach ($orderItems as $orderItem) {
                try {
                    $humcommerceTracker->addEcommerceItem(
                        $orderItem['product_reference'],
                        $orderItem['product_name'],
                        false,
                        $orderItem['product_price'],
                        $orderItem['product_quantity']
                    );
                } catch (\Exception $e) {
                }
            }

            $base_total_tax_inc = $cart->getOrderTotal(true);
            $base_total_tax_exc = $cart->getOrderTotal(false);
            $total_tax = $base_total_tax_inc - $base_total_tax_exc;

            if ($total_tax < 0) {
                $total_tax = 0;
            }
            $humcommerceTracker->doTrackEcommerceOrder(
                $order->id,
                $order->getOrdersTotalPaid(),
                $order->total_products,
                $total_tax,
                $order->total_shipping,
                $order->total_discounts
            );
        }
    }
}
